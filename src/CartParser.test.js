import CartParser from './CartParser';
import cart from '../samples/cart.json'

let parser;

beforeEach(() => {
    parser = new CartParser();
});

describe('CartParser - unit tests', () => {
    describe('validate', () => {
        it('should return error with Header type', () => {
            const content = 'Product nme,Price,Quantity\n' + 'Mollis consequat,9.00,2';

            expect(parser.validate(content)[0].type).toBe('header')
        });
        it('should not return any error cell error, when column type is unknown', () => {
            const content = 'Product name,Price,Quantity\n' + ',-9.00,2\n' + ',9.00,2\n'+ 'Melisa,9.00,2\n';
            parser.schema.columns[0].type = 'unknown';

            expect(parser.validate(content)
                .filter(error => error.type === 'cell' && error.column === 0)
                .length).toBe(0);
        });
        it('should return 3 errors with row type', () => {
            const content = 'Product name,Price,Quantity\n' + 'Mollis consequat,9.00\n'
                + 'Mollis consequat,9.00\n' + 'Mollis consequat,9.00\n';

            expect(parser.validate(content).filter(error => error.type === 'row').length).toEqual(3)
        });
        it('should return only 1 error about empty string at 0 column', () => {
            const content = 'Product name,Price,Quantity\n' + ',9.00,2\n';
            const errors = parser.validate(content);

            expect(errors[0]).toEqual({
                type: 'cell',
                row: 1,
                column: 0,
                message: 'Expected cell to be a nonempty string but received "".'
            });
            expect(errors.length).toBe(1);
        });
        it('should return only 1 error about not positive number', () => {
            const content = 'Product name,Price,Quantity\n' + 'Mollis consequat,-1,2\n' + 'Mollis consequat,9.00,2';
            const errors = parser.validate(content);

            expect(errors[0]).toEqual({
                type: 'cell',
                row: 1,
                column: 1,
                message: 'Expected cell to be a positive number but received "-1".'
            });
            expect(errors.length).toBe(1);
        });
        it('should return all types of error', () => {
            const errorTypes = ['row', 'cell', 'header'];
            const content = 'Prouct name,Price,Quantity\n' + ',-1,2\n' + 'Mollis consequa,2';
            const errors = parser.validate(content);
            const receivedErrors = errors.map(error => error.type);

            errorTypes.forEach(type => {
                expect(receivedErrors.indexOf(type)).not.toBe(-1);
            })
        });
        it('should return 0 errors', function () {
            const content = 'Product name,Price,Quantity\n' + 'Mollis consequat,9.00,2';

            expect(parser.validate(content).length).toBe(0);
        });
    });
    describe('parsreLine', () => {
        it('should return object with correct keys', () => {
            const createdObject = parser.parseLine('Mollis consequat,9.00,2');
            const keys = ['name', 'price', 'quantity', 'id'];

            Object.keys(createdObject).forEach(key => {
                expect(keys).toContain(key);
            })
            expect(Object.keys(createdObject).length).toBe(keys.length);
        });
        it('should return correct object', () => {
            const createdObject = parser.parseLine('Mollis consequat,9.00,2');

            expect(createdObject.name).toEqual('Mollis consequat');
            expect(createdObject.price).toEqual(9);
            expect(createdObject.quantity).toEqual(2);

        })
    });
    describe('parse', () => {
        it('should return error when data is not valid', () => {
            parser.readFile = jest.fn();
            parser.readFile.mockReturnValueOnce('Mollis consequat,9.00,2');

            expect(parser.parse.bind(parser)).toThrow('Validation failed!');
        });
        it('should parse valid data and calculate total', function () {
            parser.readFile = jest.fn();
            parser.readFile.mockReturnValueOnce('Product name,Price,Quantity\n' +
                'Mollis consequat,9.00,2\n' +
                'Tvoluptatem,10.32,1');
            const items = [
                {
                    name: 'Mollis consequat',
                    price: 9,
                    quantity: 2,
                },
                {
                    name: 'Tvoluptatem',
                    price: 10.32,
                    quantity: 1,
                }
            ]


            const parsed = parser.parse();

            expect(parsed.total).toBeCloseTo(28.32, 2);

            parsed.items.forEach((item, index) => {
                expect(item.id).not.toBeUndefined();

                delete item.id;
                expect(item).toEqual(items[index]);
            })
        });
    })
});

describe('CartParser - integration test', () => {
    it('should parse data correctly', function () {
        const items = cart.items.map(item => {
            delete item.id;
            return item;
        });
        const fileDir = __dirname.slice(0, -3).concat('samples\\cart.csv');
        const parsed = parser.parse(fileDir);

        expect(parsed.total).toBeCloseTo(348.32, 2);
        parsed.items.forEach((item, index) => {
            delete item.id;

            expect(item).toEqual(items[index]);
        })
    });
});
